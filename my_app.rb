class MyApp
  def initialize #auto matically called with MyApp.new
    # @view is a lambda object. Oscar's code:
    # @view = ->(env) {env.keys.sort.map {|key| "<b>#{key}</b> = #{env[key]}<br />"}} is equivilint to:
    @view = lambda {|env| env.keys.sort.map {|key| "<b>#{key}</b> = #{env[key]}<br />"}}
  end
  def call env
    [200, {"Content-Type" => "text/html"}, @view.call(env)]
  end
end

